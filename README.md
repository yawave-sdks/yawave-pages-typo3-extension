# yawave/typo3

checkout to yawave_publications in your ext-folder in your Typo3 installation

Install:
1. Install typo3
2. upload files in typo3conf/ext/yawave_publications
3. Activate extension in typo3
4. Setup integration in yawave
5. Fill connection details in typo3 backend (yawave in sidebar)
6. create pages: yawavehook, details, list
7. copy content from typo3conf/ext/yawave_publications/Configutation/Route/routeEnhancers.yaml in your site config file and parse details page id