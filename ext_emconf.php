<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Yawave',
    'description' => 'Publish your yawave publication into typo3',
    'category' => 'plugin',
    'author' => 'yawave AG',
    'author_email' => 'contact@yawave.com',
    'state' => 'stable',
    'clearCacheOnLoad' => 0,
    'version' => '2.4.8',
    'constraints' => [
        'depends' => [
            'typo3' => '10.4.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
