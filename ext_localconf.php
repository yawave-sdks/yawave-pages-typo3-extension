<?php
defined('TYPO3_MODE') || die();

(static function () {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'YawavePublications',
        'Webhook',
        [
            \Interspark\YawavePublications\Controller\ImportController::class => 'push'
        ],
        // non-cacheable actions
        [
            \Interspark\YawavePublications\Controller\ImportController::class => 'push'
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'YawavePublications',
        'PublicationList',
        [
            \Interspark\YawavePublications\Controller\PublicationController::class => 'list',
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'YawavePublications',
        'PublicationDetail',
        [
            \Interspark\YawavePublications\Controller\PublicationController::class => 'detail',
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'YawavePublications',
        'PublicationFilter',
        [
            \Interspark\YawavePublications\Controller\PublicationController::class => 'filter',
        ],
        // non-cacheable actions
        [
            \Interspark\YawavePublications\Controller\PublicationController::class => 'filter'
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'YawavePublications',
        'EventList',
        [
            \Interspark\YawavePublications\Controller\EventController::class => 'list',
        ]
    );
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'YawavePublications',
        'EventDetail',
        [
            \Interspark\YawavePublications\Controller\EventController::class => 'detail',
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'YawavePublications',
        'LiveblogDetail',
        [
            \Interspark\YawavePublications\Controller\LiveblogController::class => 'detail',
        ]
    );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    yawave_publicationlist {
                        iconIdentifier = yawave-plugin
                        title = LLL:EXT:yawave_publications/Resources/Private/Language/locallang_be.xlf:tx_yawave_publications.name
                        description = LLL:EXT:yawave_publications/Resources/Private/Language/locallang_be.xlf:tx_yawave_publications.publicationlist
                        tt_content_defValues {
                            CType = list
                            list_type = yawavepublications_publicationlist
                        }
                    }
                    yawave_publicationdetail {
                        iconIdentifier = yawave-plugin
                        title = LLL:EXT:yawave_publications/Resources/Private/Language/locallang_be.xlf:tx_yawave_publications.name
                        description = LLL:EXT:yawave_publications/Resources/Private/Language/locallang_be.xlf:tx_yawave_publications.publicationdetail
                        tt_content_defValues {
                            CType = list
                            list_type = yawavepublications_.publicationdetail
                        }
                    }
                    yawave_publicationfilter {
                        iconIdentifier = yawave-plugin
                        title = LLL:EXT:yawave_publications/Resources/Private/Language/locallang_be.xlf:tx_yawave_publications.name
                        description = LLL:EXT:yawave_publications/Resources/Private/Language/locallang_be.xlf:tx_yawave_publications.publicationfilter
                        tt_content_defValues {
                            CType = list
                            list_type = yawavepublications_publicationfilter
                        }
                    }
                    yawave_eventlist {
                        iconIdentifier = yawave-plugin
                        title = LLL:EXT:yawave_publications/Resources/Private/Language/locallang_be.xlf:tx_yawave_publications.name
                        description = LLL:EXT:yawave_publications/Resources/Private/Language/locallang_be.xlf:tx_yawave_publications.eventlist
                        tt_content_defValues {
                            CType = list
                            list_type = yawavepublications_eventlist
                        }
                    }
                    yawave_eventdetail {
                        iconIdentifier = yawave-plugin
                        title = LLL:EXT:yawave_publications/Resources/Private/Language/locallang_be.xlf:tx_yawave_publications.name
                        description = LLL:EXT:yawave_publications/Resources/Private/Language/locallang_be.xlf:tx_yawave_publications.eventdetail
                        tt_content_defValues {
                            CType = list
                            list_type = yawavepublications_eventdetail
                        }
                    }
                    yawave_liveblogdetail {
                        iconIdentifier = yawave-plugin
                        title = LLL:EXT:yawave_publications/Resources/Private/Language/locallang_be.xlf:tx_yawave_publications.name
                        description = LLL:EXT:yawave_publications/Resources/Private/Language/locallang_be.xlf:tx_yawave_publications.liveblogdetail
                        tt_content_defValues {
                            CType = list
                            list_type = yawavepublications_liveblogdetail
                        }
                    }
                }
                show = *
            }
       }'
    );

    $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
    $iconRegistry->registerIcon(
        'yawave-plugin',
        \TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider::class,
        ['source' => 'EXT:yawave_publications/Resources/Public/Icons/yawave-logo.png']
    );

})();
