<?php
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

defined('TYPO3_MODE') || die();

(function () {
    
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'YawavePublications',
        'site', // Make module a submodule of 'tools'
        'tx_YawaveConnection', // Submodule key
        '', // Position
        [
            \Interspark\YawavePublications\Controller\YawaveConnectionController::class => 'setupData, saveSettings, resetDatabase, resetPublications',
        ],
        [
            'access' => 'user,group',
            'icon' => 'EXT:yawave_publications/Resources/Public/Icons/yawave-logo.png',
            'labels' => 'LLL:EXT:yawave_publications/Resources/Private/Language/locallang_db.xlf',
        ]
    );
    
    ExtensionManagementUtility::allowTableOnStandardPages('tx_yawavepublications_domain_model_publication');
    ExtensionManagementUtility::allowTableOnStandardPages('tx_yawavepublications_domain_model_contentpart');
    ExtensionManagementUtility::allowTableOnStandardPages('tx_yawavepublications_domain_model_category');
    ExtensionManagementUtility::allowTableOnStandardPages('tx_yawavepublications_domain_model_tag');
    ExtensionManagementUtility::allowTableOnStandardPages('tx_yawavepublications_domain_model_portal');
    ExtensionManagementUtility::allowTableOnStandardPages('tx_yawavepublications_domain_model_yawaveconnection');
    ExtensionManagementUtility::allowTableOnStandardPages('tx_yawavepublications_domain_model_actiontools');
    ExtensionManagementUtility::allowTableOnStandardPages('tx_yawavepublications_domain_model_liveblogs');
    ExtensionManagementUtility::allowTableOnStandardPages('tx_yawavepublications_domain_model_liveblogentrys');
    ExtensionManagementUtility::allowTableOnStandardPages('tx_yawavepublications_domain_model_yawaveevent');
    ExtensionManagementUtility::allowTableOnStandardPages('tx_yawavepublications_domain_model_yawavecue');
    
 })();

