<?php 

namespace Interspark\YawavePublications\Provider;

class PublicationTitleProvider extends \TYPO3\CMS\Core\PageTitle\AbstractPageTitleProvider
{
	public function setTitle(string $title)
	{
		$this->title = $title;
	}
}