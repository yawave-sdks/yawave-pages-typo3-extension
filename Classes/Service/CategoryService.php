<?php
namespace Interspark\YawavePublications\Service;

use Interspark\YawavePublications\Domain\Repository\CategoryRepository;
use Interspark\YawavePublications\Domain\Repository\YawaveConnectionRepository;
use Interspark\YawavePublications\Service\YawaveService;
use Interspark\YawavePublications\Domain\Model\Category;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

class CategoryService {
	
	private $categoryRepository;
	private $yawaveService;
	private $yawaveConnectionRepository;
	private $persistenceManager;

	public function __construct(CategoryRepository $categoryRepository, 
								YawaveService $yawaveService,
								YawaveConnectionRepository $yawaveConnectionRepository,
								PersistenceManager $persistenceManager)
	{
		//controller using autowire so no configuration is needed
		$this->categoryRepository = $categoryRepository;
		$this->yawaveService = $yawaveService;
		$this->yawaveConnectionRepository = $yawaveConnectionRepository;
		$this->persistenceManager = $persistenceManager;
		
	}
	
	public function update_categories($page=0) 
	{
		
		$method = 'insert';
		$args = array();
		
		$yawave_categories_pages = $this->yawaveService->get_api_endpoint_data('https://api.yawave.com/public/multilang/applications/YAWAVE_APP_ID/categories?page=0');
		
		$configuratedlanguages = $this->yawaveConnectionRepository->findAll()->getFirst();
			
		for($page=0;$page<=$yawave_categories_pages->number_of_all_pages;$page++) {
					
			$yawave_categories = $this->yawaveService->get_api_endpoint_data('https://api.yawave.com/public/multilang/applications/YAWAVE_APP_ID/categories?page=' . $page);
			
			//DebuggerUtility::var_dump($yawave_categories);

			if ($yawave_categories && isset($yawave_categories->content) && is_array($yawave_categories->content) && sizeof($yawave_categories->content) > 0) {
				foreach ($yawave_categories->content as $category) {
					
					
					
					
					if($configuratedlanguages->getLanguageIds()) {
						
						if($category->languages) {
							
							foreach($category->languages AS $languageCode) {
											
								if (array_key_exists($languageCode, $configuratedlanguages->getLanguageIds())) {
									
									$configuratedlanguages = $this->yawaveConnectionRepository->findAll()->getFirst();
									
									if($configuratedlanguages->getLanguageIds()) {
										
										$configuratedlanguages_array = $configuratedlanguages->getLanguageIds();
										
										$args['sys_language_uid'] = $configuratedlanguages_array[$languageCode];
										
									}else{
										
										$args['sys_language_uid'] = 0;
										
									}
									
									### check if category already exists
									
									$query = $this->categoryRepository->createQuery();
									//$query->matching($query->equals('ext_id', $args['uuid']));
									$query->getQuerySettings()->setRespectStoragePage(false);
									$query->getQuerySettings()->setRespectSysLanguage(false);
									$query->getQuerySettings()->setIgnoreEnableFields(true);
									$query->matching(
										$query->logicalAnd(
											[
												$query->equals('ext_id', $category->id),
												$query->equals('sys_language_uid', $args['sys_language_uid']),
											]
										)
									);
									
									$result = $query->execute(true);
									
									if(count($result) == 0) {
										$method = 'insert';
									}else{            
										$args['typo3_uid'] = (!empty($result[0]['_LOCALIZED_UID']) && $result[0]['_LOCALIZED_UID'] > 0) ? $result[0]['_LOCALIZED_UID'] : $result[0]['uid'];
										$method = 'update';
									}
									
									##
									
									$this->save_category($category, $method, $args, $languageCode);
									
								}
								
							}
							
							if(count($category->languages) > 1) {
								
								$this->set_parent_category($category->id);
								
							}
							
						}else{
							
							$this->save_category($category, $method, $args);
							
						}
						
					}else{
						
						$this->save_category($category, $method, $args);
						
					}
					
				}
			} 
			
		}

		return true;
		
	}
	
	public function save_category($yawave_category, $method = 'insert', $args, $language = 'de')
	{
		
		$this->persistenceManager->persistAll();
		$this->persistenceManager->clearState();
		
		if($method == 'insert') {
			$category = new Category();
		}else{       
			$category = $this->categoryRepository->findByUid($args['typo3_uid']);
		}  
		
		$category->setName($yawave_category->name->$language);
		$category->setExtId($yawave_category->id);
		$category->setSlug($yawave_category->slug);
		
				
		if(!empty($yawave_category->parent_id)) {
			
			$query_parent = $this->categoryRepository->createQuery();
			$query_parent->matching($query_parent->equals('ext_id', $yawave_category->parent_id));
			$result_parent = $query_parent->execute(true);
			
			if($result_parent) {
				if($result_parent[0]['uid'] > 0) {
					$category_parent = $this->categoryRepository->findByUid($result_parent[0]['uid']);
					$category->setParent($category_parent);
				}
			}
			
		}
		
		$category->setSysLanguageUid($args['sys_language_uid']);
		$category->_setProperty('_languageUid', $args['sys_language_uid']);
		
		$this->categoryRepository->add($category);
		
		return true;
	}

	public function getAllCategories()
	{

		$query_maincategories = $this->categoryRepository->createQuery();
		$query_maincategories->matching($query_maincategories->equals('parent', 0));
		$maincategories = $query_maincategories->execute(true);



		foreach($maincategories AS $main_category) {


			$parent_categories = array();
			$query_parent = $this->categoryRepository->createQuery();
			$query_parent->matching($query_parent->equals('parent', $main_category['uid']));
			$parentcategories = $query_parent->execute(true);

			if(count($parentcategories) > 0) {

				foreach($parentcategories AS $parentcategorie) {
					$parent_categories[] = array(
						'uid' => $parentcategorie['uid'],
						'title' => $parentcategorie['name']
					);
				}

			}

			$categories[] = array(
				'uid' => $main_category['uid'],
				'title' => $main_category['name'],
				'parents' => $parent_categories,
			);

		}

		return $categories;

	}
	
	public function getSortedCategories($categories = null)
	{
	
		$query = $this->categoryRepository->createQuery();
	
		$result = $query->matching(
			$query->logicalAnd(
				[
					$query->in('uid', $categories),
					$query->equals('parent', 0),
				]
			)
		)->execute();
	
		foreach ($result AS $category) {
			$query         = $this->categoryRepository->createQuery();
			$result_childs = $query->matching(
				$query->logicalAnd(
					[
						$query->in('uid', $categories),
						$query->equals('parent', $category->getUid()),
					]
				)
			)->execute();
	
			if ($result_childs->count() > 0) {
				$category->setChilds($result_childs);
			}
	
			$return[] = $category;
	
		}
	
		return $result;
	
	}
	
	public function findParentCategory($categoryid){
	
		$query = $this->categoryRepository->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(false);
		$query->getQuerySettings()->setRespectSysLanguage(false);
		$query->getQuerySettings()->setIgnoreEnableFields(true);
		$query->matching(
			$query->logicalAnd(
				[
					$query->equals('ext_id', $categoryid),
					$query->equals('sys_language_uid', 0),
				]
			)
		);
	
		return $query->execute();
	
	}
	
	public function set_parent_category($categoryid) {
		
		$this->persistenceManager->persistAll();
		$this->persistenceManager->clearState();
		$parent_id = NULL;
		$parent_id_query = $this->findParentCategory($categoryid);
		
		
		
		if(count($parent_id_query) > 0) $parent_id = $parent_id_query[0]->getUid();
		
		
		
		###
		
		$query = $this->categoryRepository->createQuery();
		$query->getQuerySettings()->setRespectStoragePage(false);
		$query->getQuerySettings()->setRespectSysLanguage(false);
		$query->getQuerySettings()->setIgnoreEnableFields(true);
		$query->matching(
			$query->logicalAnd(
				[
					$query->equals('ext_id', $categoryid),
					$query->greaterThan('sys_language_uid', 0),
				]
			)
		);
		
		$results = $query->execute();
		
		###
		
		foreach($results AS $category) {
			
			if($category->getUid() != $parent_id) {
			
				$category->setL10nParent(($parent_id) ? $parent_id : 0); // id from original
				$category->setL10nSource(($parent_id) ? $parent_id : 0); // id from original
				$this->categoryRepository->add($category);
				
			}
			
		}
		
		###
		
		return true;
		
	}
	
}
