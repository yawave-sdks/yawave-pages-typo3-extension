<?php
namespace Interspark\YawavePublications\Service;

use Interspark\YawavePublications\Domain\Model\YawaveCue;
use Interspark\YawavePublications\Domain\Repository\YawaveCueRepository;

use Interspark\YawavePublications\Service\TagService;
use Interspark\YawavePublications\Service\CategoryService;
use Interspark\YawavePublications\Service\PortalService;
use Interspark\YawavePublications\Service\PublicationService;
use Interspark\YawavePublications\Service\YawaveService;
use Interspark\YawavePublications\Service\LiveblogsService;

use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Core\Locking\LockFactory;
use TYPO3\CMS\Core\Locking\LockingStrategyInterface;


class YawaveCueService {
	
	private   $publicationService;
	private   $tagService;
	private   $categoryService;
	private   $portalService;
	private   $yawaveCueRepository;
	private   $persistenceManager;
	private   $yawaveService;
	private   $liveblogsService;
	protected $lockFactory;
	
	public function __construct(
			YawaveCueRepository $yawaveCueRepository,
			PublicationService $publicationService,
			TagService $tagService,
			CategoryService $categoryService,
			PortalService $portalService,
			LockFactory $lockFactory,
			PersistenceManager $persistenceManager,
			YawaveService $yawaveService,
			LiveblogsService $liveblogsService
		)
	{
		$this->yawaveCueRepository 		= $yawaveCueRepository;
		$this->publicationService       = $publicationService;
		$this->tagService               = $tagService;
		$this->categoryService          = $categoryService;
		$this->portalService            = $portalService;	
		$this->lockFactory              = $lockFactory;	
		$this->persistenceManager 		= $persistenceManager;
		$this->yawaveService 			= $yawaveService;
		$this->liveblogsService 			= $liveblogsService;
	}
	
	/**
	 * save_cue
	 *
	 * @param $post, $action
	 * @return void
	 */
	
	public function save_cue($post, $action): void
	{
		
		$cue_entry_item = NULL;
		
		if ($action == 'publication' || $action == 'liveticker') {
			
			if(!empty($post->publication_uuid)) {
				$cue_entry_item = $this->yawaveCueRepository->findOneByExtId($post->publication_uuid);
			}
		
			if($cue_entry_item) {
				$cue = $cue_entry_item;
			}else{
				$cue = new YawaveCue();
			}
			
			if(!empty($post->publication_uuid)) $cue->setExtId($post->publication_uuid);
			if(!empty($post->status)) $cue->setStatus($post->status);
			if(!empty($post->event_type)) $cue->setEventType($post->event_type);	
				
			$cue->setAction($action);	
			$cue->setCuedate(date('Y-m-d H:i:s'));
			
			if($action == 'liveticker') {
				
				$cue->setApplicationUuid($post->application_uuid);
				$cue->setLiveblogUuid($post->liveblog_uuid);
				if(!empty($post->liveblog_post_uuid)) {
					$cue->setLiveblogPostUuid($post->liveblog_post_uuid);
				}
				
			}
			
			$this->yawaveCueRepository->add($cue);	
			
		}	
		
	}
	
	public function clearCue(): void
	{
		
		$locker_id = 'yavave_cue_lock_id';
		
		$capabilities = LockingStrategyInterface::LOCK_CAPABILITY_EXCLUSIVE | LockingStrategyInterface::LOCK_CAPABILITY_NOBLOCK;
		$locker       = $this->lockFactory->createLocker((string)$locker_id, $capabilities);
		
		if ($locker->acquire($capabilities)) {
		
			$this->yawaveService->set_api_token_and_app_id();
			
			$cue_items = $this->yawaveCueRepository->findAll();
			
			$query = $this->yawaveCueRepository->createQuery();
			$query->setLimit(1);
			$results = $query->execute(true);
			
			if($results) {
				
				$cue = $results[0];
				
				$action = $cue['action'];
								
				if ($action == 'publication') {
				
					$publication_uuid   = $cue['ext_id'];
					$publication_status = $cue['status']; // states from yawave: PUBLISHED | UPDATED | DELETED
					
					if (!empty($publication_uuid) && !empty($publication_status)) {
		
						$this->categoryService->update_categories();
						$this->tagService->update_tags();
						$this->portalService->update_portals();
						
						$this->publicationService->update_single_publication($publication_uuid, $publication_status);
						
						//$this->categoryService->update_categories();
		
					}
		
				} elseif ($action == 'liveticker') {
					
					$post_vars = (object) [
						'application_uuid' => $cue['application_uuid'],
						'liveblog_uuid' => $cue['liveblog_uuid'],
						'liveblog_post_uuid' => $cue['liveblog_post_uuid'],
						'event_type' => $cue['event_type']
					];
					
					$this->liveblogsService->update_liveblog_magic($post_vars);
		
				}
				
				$cue_object = $this->yawaveCueRepository->findByUid($cue['uid']); 
				$this->yawaveCueRepository->remove($cue_object);
				
				$this->persistenceManager->persistAll();
				$this->persistenceManager->clearState();
				
				
			}
		
			$locker->release();
		
		}
		
	} 
	
}
