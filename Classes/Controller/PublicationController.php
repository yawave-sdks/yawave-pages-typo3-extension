<?php

namespace Interspark\YawavePublications\Controller;

use Interspark\YawavePublications\Domain\Model\Publication;
use Interspark\YawavePublications\Domain\Repository\PublicationRepository;
use Interspark\YawavePublications\Domain\Repository\CategoryRepository;
use Interspark\YawavePublications\Domain\Repository\PortalRepository;
use Interspark\YawavePublications\Domain\Repository\TagRepository;
use Interspark\YawavePublications\Provider\PublicationTitleProvider;
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Pagination\SimplePagination;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Pagination\QueryResultPaginator;

class PublicationController extends ActionController
{
    protected $cObjectData;
    protected $configurationManager;
    protected $publicationTitleProvider;
    protected $categories;
    protected $portals;
    protected $tags;
    protected $cacheManager;

    private $publicationRepository;
    private $categoryRepository;
    private $portalRepository;
    private $tagRepository;

    public function __construct(
        ConfigurationManagerInterface $configurationManager,
        PublicationTitleProvider $publicationTitleProvider,
        PublicationRepository $publicationRepository,
        CategoryRepository $categoryRepository,
        PortalRepository $portalRepository,
        TagRepository $tagRepository,
        CacheManager $cacheManager
    ) {
        $this->configurationManager = $configurationManager;
        $this->cObjectData = $this->configurationManager->getContentObject()->data;
        $this->settings = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS);
        $this->publicationTitleProvider = $publicationTitleProvider;
        $this->cacheManager = $cacheManager;

        $this->publicationRepository = $publicationRepository;
        $this->categoryRepository = $categoryRepository;
        $this->portalRepository = $portalRepository;
        $this->tagRepository = $tagRepository;
    }

    /**
     * @return void
     */
    public function initializeAction()
    {
        parent::initializeAction();
        $this->categories = explode(',', $this->settings['categories']);
        $this->portals = explode(',', $this->settings['portals']);
        $this->tags = explode(',', $this->settings['tags']);
    }

    /**
     * @return void
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\InvalidArgumentNameException
     */
    public function initializeFilterAction()
    {
        if (!$this->request->hasArgument('overwriteDemand')){
            $foreignPluginArguments = \TYPO3\CMS\Core\Utility\GeneralUtility::_GP('tx_yawavepublications_publicationlist');
            if (!empty($foreignPluginArguments['overwriteDemand'])){
                $this->request->setArgument('overwriteDemand', $foreignPluginArguments['overwriteDemand']);
            }
        }
    }

    /**
     * Publication Filter
     *
     * @param array|null $overwriteDemand
     * @return void
     */
    public function filterAction(?array $overwriteDemand = null): void
    {
        /* find category tree */
        $categories = $this->categoryRepository->findTree($this->categories);
        /* find portals */
        $portals = $this->portals[0] != '' ? $this->portalRepository->findTree($this->portals) : $this->portalRepository->findAll();
        /* find tags */
        $tags = $this->tags[0] != '' ? $this->tagRepository->findTree($this->tags) : $this->tagRepository->findAll();

        $this->cacheManager->flushCachesByTag('yawavepublicationlist');

        $this->view->assignMultiple([
            'categories' => $categories,
            'portals' => $portals,
            'tags' => $tags,
            'overwriteDemand' => $overwriteDemand,
            'data' => $this->cObjectData,
        ]);
    }

    /**
     * Publication List
     *
     * @param array|null $overwriteDemand
     * @return void
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function listAction(?array $overwriteDemand = null): void
    {
        /* get overwrite demand */
        if ($this->settings['disableOverrideDemand'] != 1 && $overwriteDemand !== null) {
            $categoryType = $overwriteDemand['categoryType'] ?? 'categories';
            if (isset($overwriteDemand[$categoryType])) {
                foreach ($overwriteDemand[$categoryType] as $key => $category) {
                    if ($category == '') {
                        unset($overwriteDemand[$categoryType][$key]);
                    }
                }
                $this->categories = $overwriteDemand[$categoryType];
            }
            if (isset($overwriteDemand['portals']))
                $this->portals = $overwriteDemand['portals'];
            if (isset($overwriteDemand['tags']))
                $this->tags = $overwriteDemand['tags'];
        }

        /* find publications */
        $settings = $this->request->hasArgument('settings') ? $this->request->getArgument('settings') : $this->settings;
        $categories = $this->request->hasArgument('categories') ? $this->request->getArgument('categories') : $this->categories;
        $categoriesBySlug = $this->request->hasArgument('categoriesBySlug') ? $this->request->getArgument('categoriesBySlug') : [];
        $portals = $this->request->hasArgument('portals') ? $this->request->getArgument('portals') : $this->portals;
        $tags = $this->request->hasArgument('tags') ? $this->request->getArgument('tags') : $this->tags;
        $publications = $this->publicationRepository->findPublications($settings, $categories, $categoriesBySlug, $portals, $tags);

        /* pagination */
        $currentPage = $this->request->hasArgument('currentPage') ? $this->request->getArgument('currentPage') : 1;
        $itemsPerPage = $this->settings['itemsPerPage'] ?: 3;
        $paginator = new QueryResultPaginator($publications, $currentPage, $itemsPerPage);
        $simplePagination = new SimplePagination($paginator);
        $pagination = $this->buildSimplePagination($simplePagination, $paginator);

        /* cache list */
        $GLOBALS['TSFE']->addCacheTags(['yawavepublicationlist']);

        $this->view->assignMultiple([
            'settings' => $settings,
            'publications' => $publications,
            'overwriteDemand' => $overwriteDemand,
            'paginator' => $paginator,
            'pagination' => $pagination,
            'data' => $this->cObjectData,
        ]);
    }

    /**
     * Publication Detail
     *
     * @param \Interspark\YawavePublications\Domain\Model\Publication|null $publication
     * @return void
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     */
    public function detailAction(?Publication $publication = null): void
    {
        /* display single publication selected in flex form */
        if ($publication == null && (int)$this->settings['singlePublication'] > 0) {
            $publication = $this->publicationRepository->findByUid((int)$this->settings['singlePublication']);
            //$this->redirect('detail', null, null, ['publication' => $publication]);
        }

        /* return if no publication was found */
        if ($publication == null) return;

        /* cache detail */
        $GLOBALS['TSFE']->addCacheTags(['yawavepublicationdetail', 'yawavepublication_'.$publication->getExtId()]);

        /* set publication title */
        $this->publicationTitleProvider->setTitle($publication->getTitle());

        $this->view->assignMultiple([
            'publication' => $publication,
            'data' => $this->cObjectData,
        ]);
    }

    /**
     * Build simple pagination
     *
     * @param SimplePagination $simplePagination
     * @param QueryResultPaginator $paginator
     * @return array
     */
    protected function buildSimplePagination(SimplePagination $simplePagination, QueryResultPaginator $paginator): array
    {
        $firstPage = $simplePagination->getFirstPageNumber();
        $lastPage = $simplePagination->getLastPageNumber();

        return [
            'lastPageNumber' => $lastPage,
            'firstPageNumber' => $firstPage,
            'nextPageNumber' => $simplePagination->getNextPageNumber(),
            'previousPageNumber' => $simplePagination->getPreviousPageNumber(),
            'startRecordNumber' => $simplePagination->getStartRecordNumber(),
            'endRecordNumber' => $simplePagination->getEndRecordNumber(),
            'currentPageNumber' => $paginator->getCurrentPageNumber(),
            'pages' => range($firstPage, $lastPage)
        ];
    }
}
