<?php

namespace Interspark\YawavePublications\Controller;

use Interspark\YawavePublications\Domain\Model\LiveblogEntrys;
use Interspark\YawavePublications\Domain\Model\Liveblogs;
use Interspark\YawavePublications\Domain\Repository\LiveblogsRepository;
use Interspark\YawavePublications\Provider\PublicationTitleProvider;
use TYPO3\CMS\Core\Pagination\SimplePagination;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Core\Pagination\ArrayPaginator;

class LiveblogController extends ActionController
{
    protected $cObjectData;
    protected $configurationManager;
    protected $publicationTitleProvider;

    private $liveblogsRepository;

    public function __construct(
        ConfigurationManagerInterface $configurationManager,
        PublicationTitleProvider $publicationTitleProvider,
        LiveblogsRepository $liveblogsRepository
    ) {
        $this->configurationManager = $configurationManager;
        $this->cObjectData = $this->configurationManager->getContentObject()->data;
        $this->settings = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS);
        $this->publicationTitleProvider = $publicationTitleProvider;

        $this->liveblogsRepository = $liveblogsRepository;
    }

    /**
     * Event Detail
     *
     * @param ?Liveblogs $liveblog
     * @return void
     */
    public function detailAction(?Liveblogs $liveblog = null): void
    {
        /* display liveblog selected in flex form */
        if ($liveblog == null && (int)$this->settings['singleLiveblog'] > 0) {
            $liveblog = $this->liveblogsRepository->findByUid((int)$this->settings['singleLiveblog']);
            //$this->redirect('detail', null, null, ['liveblog' => $liveblog]);
        }

        /* display liveblog by gameExtId */
        if ($liveblog == null && $this->request->hasArgument('gameExtId')) {
            $liveblog = $this->liveblogsRepository->findBySportradarId($this->request->getArgument('gameExtId'));
            //$this->redirect('detail', null, null, ['liveblog' => $liveblog]);
        }

        /* return if no liveblog was found */
        if ($liveblog == null) return;

        /* sort liveblog entries */
        $entries = $liveblog->getEntrys()->toArray();
        if (count($entries) > 0) {
            if ($this->settings['orderDirection'] == 'asc') {
                \usort($entries, function (LiveblogEntrys $b, LiveblogEntrys $a){
                    if($a->getTimelineTimestamp() === $b->getTimelineTimestamp()){
                        return $a->getCreateDate() === $b->getCreateDate() ? $a->getUid() < $b->getUid() : $a->getCreateDate() < $b->getCreateDate();
                    }
                    return $a->getTimelineTimestamp() < $b->getTimelineTimestamp();
                });
            } else {
                \usort($entries, function (LiveblogEntrys $a, LiveblogEntrys $b){
                    if($a->getTimelineTimestamp() === $b->getTimelineTimestamp()){
                        return $a->getCreateDate() === $b->getCreateDate() ? $a->getUid() < $b->getUid() : $a->getCreateDate() < $b->getCreateDate();
                    }
                    return $a->getTimelineTimestamp() < $b->getTimelineTimestamp();
                });
            }
        }

        /* pagination */
        $currentPage = $this->request->hasArgument('currentPage') ? $this->request->getArgument('currentPage') : 1;
        $itemsPerPage = $this->settings['itemsPerPage'] ?: 3;
        $paginator = new ArrayPaginator($entries, $currentPage, $itemsPerPage);
        $simplePagination = new SimplePagination($paginator);
        $pagination = $this->buildSimplePagination($simplePagination, $paginator);

        /* cache detail */
        //$GLOBALS['TSFE']->addCacheTags(['yawaveliveblog', 'yawaveliveblog_'.$liveblog->getExtId()]);

        /* set publication title */
        $this->publicationTitleProvider->setTitle($liveblog->getTitle());

        $this->view->assignMultiple([
            'liveblog' => $liveblog,
            'entries' => $entries,
            'paginator' => $paginator,
            'pagination' => $pagination,
            'data' => $this->cObjectData,
        ]);
    }

    /**
     * Build simple pagination
     *
     * @param SimplePagination $simplePagination
     * @param ArrayPaginator $paginator
     * @return array
     */
    protected function buildSimplePagination(SimplePagination $simplePagination, ArrayPaginator $paginator): array
    {
        $firstPage = $simplePagination->getFirstPageNumber();
        $lastPage = $simplePagination->getLastPageNumber();

        return [
            'lastPageNumber' => $lastPage,
            'firstPageNumber' => $firstPage,
            'nextPageNumber' => $simplePagination->getNextPageNumber(),
            'previousPageNumber' => $simplePagination->getPreviousPageNumber(),
            'startRecordNumber' => $simplePagination->getStartRecordNumber(),
            'endRecordNumber' => $simplePagination->getEndRecordNumber(),
            'currentPageNumber' => $paginator->getCurrentPageNumber(),
            'pages' => range($firstPage, $lastPage)
        ];
    }
    
}
