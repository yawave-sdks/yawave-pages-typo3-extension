<?php

namespace Interspark\YawavePublications\Controller;

use Interspark\YawavePublications\Service\YawaveService;
use Interspark\YawavePublications\Service\PublicationService;
use Interspark\YawavePublications\Service\TagService;
use Interspark\YawavePublications\Service\CategoryService;
use Interspark\YawavePublications\Service\YawaveCueService;
use Interspark\YawavePublications\Service\PortalService;
use Interspark\YawavePublications\Service\LiveblogsService;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class ImportController extends ActionController
{
    private   $yawaveService;
    private   $publicationService;
    private   $tagService;
    private   $categoryService;
    private   $portalService;
    private   $liveblogsService;
    private   $yawaveCueService;

    public function __construct(
        YawaveService $yawaveService,
        PublicationService $publicationService,
        TagService $tagService,
        CategoryService $categoryService,
        PortalService $portalService,
        LiveblogsService $liveblogsService,
        YawaveCueService $yawaveCueService
    ) {
        $this->yawaveService = $yawaveService;
        $this->publicationService = $publicationService;
        $this->tagService = $tagService;
        $this->categoryService = $categoryService;
        $this->portalService = $portalService;
        $this->liveblogsService = $liveblogsService;
        $this->yawaveCueService = $yawaveCueService;

    }

    public function pushAction(): string
    {
        
        $action         = GeneralUtility::_GET('action');
        $body           = file_get_contents("php://input");
        $post_vars      = json_decode($body);
        
        if($this->yawaveService->get_mutexcue_status() == 1) {
            $this->yawaveCueService->save_cue($post_vars, $action);
            return true;
        }else{
            
            $this->yawaveService->set_api_token_and_app_id();
                            
            if ($action == 'publication') {
            
                $publication_uuid   = $post_vars->publication_uuid;
                $publication_status = $post_vars->status; // states from yawave: PUBLISHED | UPDATED | DELETED
            
                if (!empty($publication_uuid) && !empty($publication_status)) {
            
                    $this->categoryService->update_categories();
                    $this->tagService->update_tags();
                    $this->portalService->update_portals();
            
                    $this->publicationService->update_single_publication($publication_uuid, $publication_status);
            
                    //$this->categoryService->update_categories();
            
                    return 'publication done';
            
                } else {
            
                    return false;
            
                }
            
            } elseif ($action == 'liveticker') {
            
                $this->liveblogsService->update_liveblog_magic($post_vars);
                return 'liveticker done';
            
            } else {
            
                return false;
            
            }
        }
        
    }
    
}
