<?php

namespace Interspark\YawavePublications\Controller;

use Interspark\YawavePublications\Domain\Model\Publication;
use Interspark\YawavePublications\Domain\Repository\PublicationRepository;
use Interspark\YawavePublications\Domain\Repository\YawaveEventRepository;
use Interspark\YawavePublications\Provider\PublicationTitleProvider;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

class EventController extends ActionController
{
    protected $cObjectData;
    protected $configurationManager;
    protected $publicationTitleProvider;
    protected $categories;
    protected $portals;

    private $publicationRepository;
    private $eventRepository;

    public function __construct(
        ConfigurationManagerInterface $configurationManager,
        PublicationTitleProvider $publicationTitleProvider,
        PublicationRepository $publicationRepository,
        YawaveEventRepository $eventRepository
    ) {
        $this->configurationManager = $configurationManager;
        $this->cObjectData = $this->configurationManager->getContentObject()->data;
        $this->settings = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS);
        $this->publicationTitleProvider = $publicationTitleProvider;

        $this->publicationRepository = $publicationRepository;
        $this->eventRepository = $eventRepository;
    }

    /**
     * @return void
     */
    public function initializeAction()
    {
        parent::initializeAction();
        $this->categories = explode(',', $this->settings['categories']);
        $this->portals = explode(',', $this->settings['portals']);
    }
    
    /**
     * Event List
     *
     * @return void
     */
    public function listAction(): void
    {
        /* return if no category and no portal was selected in flex form */
        if ($this->categories[0] == '' && $this->portals[0] == '') return;

        /* find all events */
        $events = $this->eventRepository->findAllEvents($this->settings);

        $i = 0;
        $publications = [];

        /* find event-publications, group by month and limit if defined in flex form */
        foreach ($events as $event) {
            $publication = $this->publicationRepository->findPublicationByEvent($this->settings, $this->categories, $this->portals, $event->getPublicationId())->getFirst();
            if ($publication) {
                if ($this->settings['groupByMonth']) {
                    $publications[(int)date('ym', strtotime($event->getEventStart()))][] = $publication;
                } else {
                    $publications[] = $publication;
                }
                $i++;
            }
            if ((int)$this->settings['limit'] == $i) break;
        }

        $this->view->assignMultiple([
            'events' => $publications,
            'data' => $this->cObjectData,
        ]);
    }

    /**
     * Event Detail
     *
     * @param ?Publication $publication
     * @return void
     */
    public function detailAction(?Publication $publication = null): void
    {
        /* display single event selected in flex form */
        if ($publication == null && (int)$this->settings['singleEvent'] > 0) {
            $event = $this->eventRepository->findByUid((int)$this->settings['singleEvent']);
            $publication = $this->publicationRepository->findPublicationByEvent($this->settings, $this->categories, $this->portals, $event->getPublicationId())->getFirst();
            //$this->redirect('detail', null, null, ['publication' => $publication]);
        }

        /* return if no publication was found */
        if ($publication == null) return;

        /* cache detail */
        $GLOBALS['TSFE']->addCacheTags(['yawaveeventdetail', 'yawaveevent_'.$publication->getExtId()]);

        /* set publication title */
        $this->publicationTitleProvider->setTitle($publication->getTitle());
        
        $this->view->assignMultiple([
            'publication' => $publication,
            'data' => $this->cObjectData,
        ]);
    }
    
}
