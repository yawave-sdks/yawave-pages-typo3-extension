<?php
namespace Interspark\YawavePublications\Command;

use Interspark\YawavePublications\Service\YawaveCueService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


class CueCommand extends Command {
    protected static $name = 'yawave:cue:clear';

    /** @var YawaveCueService */
    private $service;

    public function __construct(YawaveCueService $service)
    {
        $this->service = $service;
        parent::__construct(self::$name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int {
        $this->service->clearCue();
        $io = new SymfonyStyle($input, $output);
        $io->writeln('cleared.');
        return 0;
    }
}