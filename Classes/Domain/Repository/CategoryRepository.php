<?php

declare(strict_types=1);

namespace Interspark\YawavePublications\Domain\Repository;

/**
 * This file is part of the "Yawave Adapter" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2021 Hannes Pries <hannes.pries@interspark.com>, Interspark GmbH
 */

/**
 * The repository for Categories
 */
class CategoryRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * Find category tree
     *
     * @param array $categories
     * @return array
     */
    public function findTree(array $categories): array
    {
        if ($categories[0] == '') return [];

        $query = $this->createQuery();

        $constraints = [];
        foreach ($categories as $category) {
            $constraints[] = $query->equals('uid', $category);
        }
        $results = $query->matching($query->logicalOr($constraints))->execute();

        $tree = [];
        $flatCategories = [];
        /** @var \Interspark\YawavePublications\Domain\Model\Category $category */
        foreach ($results as $category) {
            $flatCategories[$category->getUid()] = [
                'item' => $category,
                'parent' => ($category->getParent()) ? $category->getParent()->getUid() : null
            ];
        }
        foreach ($flatCategories as $flatCategory) {
            if (!isset($flatCategories[$flatCategory['parent']])) {
                $flatCategory['parent'] = null;
            }
        }
        foreach ($flatCategories as $id => &$node) {
            if ($node['parent'] === null) {
                $tree[$id] = &$node;
            } else {
                $flatCategories[$node['parent']]['children'][$id] = &$node;
            }
        }

        return $tree;
    }
}
