<?php

declare(strict_types=1);

namespace Interspark\YawavePublications\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;

/**
 * This file is part of the "Yawave Adapter" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2021 Hannes Pries <hannes.pries@interspark.com>, Interspark GmbH
 */

/**
 * The repository for Tags
 */
class TagRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    protected $defaultOrderings = [
        'name' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * Find tag tree
     *
     * @param array $tags
     * @return QueryResultInterface
     */
    public function findTree(array $tags): QueryResultInterface
    {
        $query = $this->createQuery();

        $constraints = [];

        if ($tags[0] != '') {
            foreach ($tags as $tag) {
                $constraints[] = $query->equals('uid', $tag);
            }
        }

        return $query->matching($query->logicalOr($constraints))->execute();
    }
}
