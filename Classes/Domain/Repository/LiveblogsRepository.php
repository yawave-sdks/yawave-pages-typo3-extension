<?php

declare(strict_types=1);

namespace Interspark\YawavePublications\Domain\Repository;

/**
 * This file is part of the "Yawave Adapter" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2021 Hannes Pries <hannes.pries@interspark.com>, Interspark GmbH
 */

/**
 * The repository for Tags
 */
class LiveblogsRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @param $gameExtId
     * @return object|null
     */
    public function findBySportradarId($gameExtId): ?object
    {
        $query = $this->createQuery();

        $constraints = [
            $query->equals('source_language_code', $GLOBALS['TSFE']->language->getTwoLetterIsoCode()),
            $query->equals('sportradar_id', $gameExtId)
        ];

        return $query->matching($query->logicalAnd($constraints))->execute()->getFirst();
    }

}
