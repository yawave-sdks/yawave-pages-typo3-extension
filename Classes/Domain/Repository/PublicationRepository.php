<?php
declare(strict_types=1);

namespace Interspark\YawavePublications\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * This file is part of the "Yawave Adapter" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2021 Hannes Pries <hannes.pries@interspark.com>, Interspark GmbH
 */

/**
 * The repository for Publications
 */
class PublicationRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    protected $defaultOrderings = ['begin_date' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING];

    private $categoryRepository;

    /**
     * Inject the category repository
     *
     * @param \Interspark\YawavePublications\Domain\Repository\CategoryRepository $categoryRepository
     */
    public function injectCategoryRepository(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Returns all objects of this repository.
     *
     * @param int $limit
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     * @api
     */
    public function findAll(int $limit = 0): QueryResultInterface
    {
        $query = $this->createQuery();
        if ($limit > 0) {
            $query->setLimit($limit);
        }
        return $query->execute();
    }


    /**
     * @param array $settings
     * @param array $categories
     * @param array $categoriesBySlug
     * @param array $portals
     * @param array $tags
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findPublications(array $settings, array $categories, array $categoriesBySlug, array $portals, array $tags): QueryResultInterface
    {
        $query = $this->createQuery();

        if (isset($settings['orderBy']) && isset($settings['orderDirection'])) {
            $query->setOrderings([$settings['orderBy'] => strtoupper($settings['orderDirection'])]);
        }
        if ((bool)$settings['limit']) {
            $query->setLimit((int)$settings['limit']);
        }

        $constraints = [
            $query->equals('sysLanguageUid', $GLOBALS['TSFE']->language->getLanguageId()),
        ];

        /* add categoriesBySlug to categories */
        foreach ($categoriesBySlug as $categoryBySlug) {
            $categories[] = $this->categoryRepository->findOneBySlug($categoryBySlug);
        }

        if ($settings['categoryConjunction'] == 'andor') {
            /* and/or filter */
            $categoryTree   = $this->categoryRepository->findTree($categories);
            $subConstraints = [];
            foreach ($categoryTree as $parentUid => $parent) {
                if (isset($parent['children']) && sizeof($parent['children']) > 0) {
                    $childConstraints = [];
                    foreach ($parent['children'] as $child) {
                        $childConstraints[] = $query->contains('categories', $child['item']->getUid());
                    }
                    $subConstraints[$parentUid] = $query->logicalOr($childConstraints);
                }
            }
            if (sizeof($subConstraints) <= 0) {
                foreach ($categoryTree as $category) {
                    $subConstraints[] = $query->contains('categories', $category);
                }
            }
            $constraints[] = $query->logicalAnd($subConstraints);
        } else {
            /* and & or filter */
            $subConstraints = [];
            foreach ($categories as $category) {
                if ($category != '') {
                    $subConstraints[] = $query->contains('categories', $category);
                }
            }
            if (!empty($subConstraints)) {
                if ($settings['categoryConjunction'] == 'and') {
                    $constraints[] = $query->logicalAnd($subConstraints);
                } else {
                    $constraints[] = $query->logicalOr($subConstraints);
                }
            }
        }
        if ($portals[0] != '') {
            $subConstraints = [];
            foreach ($portals as $portal) {
                $subConstraints[] = $query->contains('portals', $portal);
            }
            $constraints[] = $query->logicalOr($subConstraints);
        }
        if ($tags[0] != '') {
            $subConstraints = [];
            foreach ($tags as $tag) {
                $subConstraints[] = $query->contains('tags', $tag);
            }
            $constraints[] = $query->logicalOr($subConstraints);
        }

        return $query->matching($query->logicalAnd($constraints))->execute();
    }


    /**
     * Events
     *
     * @param array $settings
     * @param array $categories
     * @param array $portals
     * @param string $extId
     * @return \TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findPublicationByEvent(array $settings, array $categories, array $portals, string $extId): QueryResultInterface
    {
        $query = $this->createQuery();
        if (isset($settings['orderBy']) && isset($settings['orderDirection'])) {
            $query->setOrderings([$settings['orderBy'] => strtoupper($settings['orderDirection'])]);
        }

        $constraints = [
            $query->equals('extId', $extId),
            $query->equals('type', 'EVENT'),
        ];

        if ($categories[0] != '') {
            $subConstraints = [];
            foreach ($categories as $category) {
                $subConstraints[] = $query->contains('categories', $category);
            }
            if ($settings['categoryConjunction'] == 'and') {
                $constraints[] = $query->logicalAnd($subConstraints);
            } else {
                $constraints[] = $query->logicalOr($subConstraints);
            }
        }
        if ($portals != '') {
            $subConstraints = [];
            foreach ($portals as $portal) {
                $subConstraints[] = $query->contains('portals', $portal);
            }
            $constraints[] = $query->logicalOr($subConstraints);
        }

        return $query->matching($query->logicalAnd($constraints))->execute();
    }

}
