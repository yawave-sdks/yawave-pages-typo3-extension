<?php

declare(strict_types=1);

namespace Interspark\YawavePublications\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;

/**
 * This file is part of the "Yawave Adapter" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2021 Hannes Pries <hannes.pries@interspark.com>, Interspark GmbH
 */

/**
 * The repository for Portals
 */
class PortalRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    protected $defaultOrderings = [
        'title' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * Find portal tree
     *
     * @param array $portals
     * @return QueryResultInterface
     */
    public function findTree(array $portals): QueryResultInterface
    {
        $query = $this->createQuery();

        $constraints = [];

        if ($portals[0] != '') {
            foreach ($portals as $portal) {
                $constraints[] = $query->equals('uid', $portal);
            }
        }

        return $query->matching($query->logicalOr($constraints))->execute();
    }
}
