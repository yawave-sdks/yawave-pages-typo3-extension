<?php
declare(strict_types=1);

namespace Interspark\YawavePublications\Domain\Repository;

use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;

/**
 * This file is part of the "Yawave Adapter" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2021 Hannes Pries <hannes.pries@interspark.com>, Interspark GmbH
 */

/**
 * The repository for Events
 */
class YawaveEventRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    public function findAllEvents(array $settings): QueryResultInterface
    {
        $query = $this->createQuery();
        $query->setOrderings(['event_start' => strtoupper($settings['orderDirection'])]);

        return $query->matching(
            $query->greaterThanOrEqual('event_start', date('Y-m-d H:i:s'))
        )->execute();
    }

}
