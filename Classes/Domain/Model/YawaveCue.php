<?php

declare(strict_types=1);

namespace Interspark\YawavePublications\Domain\Model;


/**
 * This file is part of the "Yawave Adapter" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2021 Hannes Pries <hannes.pries@interspark.com>, Interspark GmbH
 */

/**
 * YawaveEvent
 */
class YawaveCue extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

	/**
	 * ext_id
	 *
	 * @var string
	 */
	protected $extId = '';
	
	/**
	 * action
	 *
	 * @var string
	 */
	protected $action = '';
	
	/**
	 * status
	 *
	 * @var string
	 */
	protected $status = '';
	
	/**
	 * event_type
	 *
	 * @var string
	 */
	protected $eventType = '';
	
	/**
	 * application_uuid
	 *
	 * @var string
	 */
	protected $applicationUuid = '';
	
	/**
	 * liveblog_uuid
	 *
	 * @var string
	 */
	protected $liveblogUuid = '';
	
	/**
	 * liveblog_post_uuid
	 *
	 * @var string
	 */
	protected $liveblogPostUuid = '';
	
	/**
	 * cuedate
	 *
	 * @var string
	 */
	protected $cuedate = '';
	
	
	/**
	 * Returns the ext_id
	 *
	 * @return string $extId
	 */
	public function getExtId()
	{
		return $this->extId;
	}
	
	/**
	 * Sets the ext_id
	 *
	 * @param string $extId
	 * @return void
	 */
	public function setExtId(string $extId)
	{
		$this->extId = $extId;
	}
	
	/**
	 * Returns the action
	 *
	 * @return string action
	 */
	public function getAction()
	{
		return $this->action;
	}
	
	/**
	 * Sets the action
	 *
	 * @param string $action
	 * @return void
	 */
	public function setAction(string $action)
	{
		$this->action = $action;
	}
	
	/**
	 * Returns the status
	 *
	 * @return string status
	 */
	public function getStatus()
	{
		return $this->status;
	}
	
	/**
	 * Sets the status
	 *
	 * @param string $status
	 * @return void
	 */
	public function setStatus(string $status)
	{
		$this->status = $status;
	}
	
	/**
	 * Returns the event_type
	 *
	 * @return string eventType
	 */
	public function getEventType()
	{
		return $this->eventType;
	}
	
	/**
	 * Sets the event_type
	 *
	 * @param string $eventType
	 * @return void
	 */
	public function setEventType(string $eventType)
	{
		$this->eventType = $eventType;
	}
	
	/**
	 * Returns the application_uuid
	 *
	 * @return string applicationUuid
	 */
	public function getApplicationUuid()
	{
		return $this->applicationUuid;
	}
	
	/**
	 * Sets the application_uuid
	 *
	 * @param string $applicationUuid
	 * @return void
	 */
	public function setApplicationUuid(string $applicationUuid)
	{
		$this->applicationUuid = $applicationUuid;
	}
	
	/**
	 * Returns the liveblog_uuid
	 *
	 * @return string liveblogUuid
	 */
	public function getLiveblogUuid()
	{
		return $this->liveblogUuid;
	}
	
	/**
	 * Sets the liveblog_uuid
	 *
	 * @param string $liveblogUuid
	 * @return void
	 */
	public function setLiveblogUuid(string $liveblogUuid)
	{
		$this->liveblogUuid = $liveblogUuid;
	}
	
	
	/**
	 * Returns the liveblog_post_uuid
	 *
	 * @return string liveblogPostUuid
	 */
	public function getLiveblogPostUuid()
	{
		return $this->liveblogPostUuid;
	}
	
	/**
	 * Sets the liveblog_post_uuid
	 *
	 * @param string $liveblogPostUuid
	 * @return void
	 */
	public function setLiveblogPostUuid(string $liveblogPostUuid)
	{
		$this->liveblogPostUuid = $liveblogPostUuid;
	}
	
	/**
	 * Returns the cuedate
	 *
	 * @return string cuedate
	 */
	public function getCuedate()
	{
		return $this->cuedate;
	}
	
	/**
	 * Sets the cuedate
	 *
	 * @param string $cuedate
	 * @return void
	 */
	public function setCuedate(string $cuedate)
	{
		$this->cuedate = $cuedate;
	}
	
	

}
