<?php
defined('TYPO3_MODE') || die();

$extName = 'YawavePublications';
$extKey = 'yawave_publications';
$plugins = [
    'Webhook' => 'Yawave Webhook',
    'PublicationDetail' => 'Yawave Publication Detail',
    'PublicationList' => 'Yawave Publication List',
    'PublicationFilter' => 'Yawave Publication Filter',
    'EventList' => 'Yawave Event List',
    'EventDetail' => 'Yawave Event Detail',
    'LiveblogDetail' => 'Yawave Liveblog Detail',
];

foreach ($plugins as $plugin => $name) {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin($extName, $plugin, $name);
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][str_replace('_', '', $extKey) . '_' . strtolower($plugin)] = 'recursive';
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][str_replace('_', '', $extKey) . '_' . strtolower($plugin)] = 'pi_flexform';
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(str_replace('_', '', $extKey) . '_' . strtolower($plugin), 'FILE:EXT:' . $extKey . '/Configuration/FlexForms/' . str_replace('_', '', $extKey) . '_' . strtolower($plugin) . '.xml');
}
